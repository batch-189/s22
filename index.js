/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];
console.log(friendsList.length)

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.

*/          function newRegistered(register){
                
                  
                console.log("Registered: " + register)
                if(registeredUsers == register){
                    alert("Registration failed. Username already exists!")
                }else{
                    registeredUsers.push(register)
                    alert("Thank you for registering")
                    console.log(registeredUsers)
                }            



            }
    
            newRegistered("Brian O. Brien")


/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.

*/      function newFriendlist(friend){
            
                
                friendsList = friend
                console.log(`Registered: ${friendsList}`)
                console.log(friendsList.length)
                if(registeredUsers.includes(friendsList)){
                    alert(`You have added <registeredUser> as a friend!`)
                    console.log(registeredUsers)
                }else{
                    alert(`User not found.`)
                }
                


        }
        newFriendlist(`Akiko Yukihime`)


/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/          function showFriendList(){
                    if(friendsList == 0){
                        alert(`You currently have 0 friends. Add one first.`)
                }
        }
            showFriendList()


/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.
*/

            function amountFriendList(){
                if(friendsList <= 0){
                  alert(`You currently have 0 friends. Add one first.`)
                }else {
                    alert(`You currently have <numberOfFriends> friends.`)
                }            
        }
        amountFriendList()
/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - In the browser console, log the friendsList array.

*/             function deletedUsers(){
                    friendsList = friendsList.length - 1
                    console.log(friendsList)
            }
            deletedUsers()


/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

                function superdeleter(superdelete,deletesupreme){
                        let deleteMaster = registeredUsers.splice(superdelete,deletesupreme)

                        console.log(`Deleted: ${deleteMaster}`)
                        console.log(registeredUsers)

                }
                superdeleter(0,1)
